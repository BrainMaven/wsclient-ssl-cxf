package com.huliang.cxf.ssl.client;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.huliang.cxf.ssl.helloword.HelloWordFacade;

/**
 * CXF SLL客户端
 * 
 * @author huliang
 * @version $Id: Client.java, v 0.1 2013年8月11日 下午6:51:45 huliang Exp $
 */
public class Client {
    
    /**
     * 
     * @param args
     */
    public static void main(String[] args) {
        //System.setProperty("sun.security.ssl.allowUnsafeRenegotiation", "true");
        //System.setProperty("sun.security.ssl.allowLegacyHelloMessages", "true");
        
//        System.setProperty("javax.net.ssl.trustStore", "C:/deve/keys/ssl/client.truststore");  
//        System.setProperty("javax.net.ssl.trustStorePassword","123456");
//        System.setProperty("javax.net.ssl.trustStoreType","JKS");
//        
//        System.setProperty("javax.net.ssl.keyStoreType","PKCS12") ;  
//        System.setProperty("javax.net.ssl.keyStore","C:/deve/keys/ssl/browser.p12") ;  
//        System.setProperty("javax.net.ssl.keyStorePassword","123456") ;
        
        /*显示详细日志信息*/
        System.setProperty("javax.net.debug", "all");
        
        ApplicationContext context = new ClassPathXmlApplicationContext("classpath:META-INF/spring/cxf-client-ssl.xml");
        HelloWordFacade helloWord = (HelloWordFacade) context.getBean("helloWorldFacade");
        System.out.println(helloWord.sayHello("CXF SLL"));
    }
}
